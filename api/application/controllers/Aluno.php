<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aluno extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

	public function index()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
		    $response = $this->MyModel->auth();
		    if($response['status'] == 200){
				$params = json_decode(file_get_contents("php://input"));
				if($params->matricula != null){
					$resp = $this->MyModel->getAlunoByMatricula($params->matricula);
				}else{
					$resp = array('error'=>404,'description'=>'O código da matricula não foi encontrado!');
				}
	    		json_output($response['status'],$resp);
			}
		}
	}

}
