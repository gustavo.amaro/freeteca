-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Jun-2019 às 13:31
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `freeteca`
--
CREATE DATABASE IF NOT EXISTS `freeteca` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `freeteca`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `devolucao`
--

CREATE TABLE `devolucao` (
  `id` int(11) NOT NULL,
  `dataDevolucao` date DEFAULT NULL,
  `emprestimo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `emprestimo`
--

CREATE TABLE `emprestimo` (
  `id` int(11) NOT NULL,
  `matriculaAluno` varchar(100) DEFAULT NULL,
  `dataEmprestimo` date DEFAULT NULL,
  `dataDevolucao` date DEFAULT NULL,
  `devolvido` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `emprestimo_devolucao`
-- (See below for the actual view)
--
CREATE TABLE `emprestimo_devolucao` (
`emprestimo` int(11)
,`previsao` date
,`devolucao` date
,`matricula` varchar(100)
,`dataEmprestimo` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `emprestimo_livros`
-- (See below for the actual view)
--
CREATE TABLE `emprestimo_livros` (
`cod` int(11)
,`titulo` varchar(200)
,`autor` varchar(200)
,`editora` varchar(200)
,`ano` int(11)
,`categoria` varchar(100)
,`estoque` int(11)
,`emprestimo` int(11)
,`matricula` varchar(100)
,`dataEmprestimo` date
,`dataDevolucao` date
,`devolvido` tinyint(1)
);

-- --------------------------------------------------------

--
-- Estrutura da tabela `itememprestimo`
--

CREATE TABLE `itememprestimo` (
  `id` int(11) NOT NULL,
  `livro` int(11) DEFAULT NULL,
  `emprestimo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro`
--

CREATE TABLE `livro` (
  `codLivro` int(11) NOT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `autor` varchar(200) DEFAULT NULL,
  `editora` varchar(200) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `categoria` varchar(100) DEFAULT NULL,
  `qtdPaginas` int(11) DEFAULT NULL,
  `estoque` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `livro`
--

INSERT INTO `livro` (`codLivro`, `titulo`, `autor`, `editora`, `ano`, `categoria`, `qtdPaginas`, `estoque`) VALUES
(2, 'Os lusíadas', 'Luiz de Camões', 'Atica', 1975, 'Romance', 370, 19),
(3, 'Robinson Crusoe', 'Daniel Defoe', 'Desconhecida', 1719, 'Romance', 20, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `multa`
--

CREATE TABLE `multa` (
  `id` int(11) NOT NULL,
  `valor` decimal(8,2) NOT NULL,
  `matriculaAluno` varchar(100) DEFAULT NULL,
  `paga` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `renovacao`
--

CREATE TABLE `renovacao` (
  `id` int(11) NOT NULL,
  `dataRenovacao` date DEFAULT NULL,
  `emprestimo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nomeUsuario` varchar(100) DEFAULT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `senha` varchar(300) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `funcao` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nomeUsuario`, `cpf`, `senha`, `nome`, `funcao`) VALUES
(3, 'gerente', '22222222222', '123', 'Teste Gerente', 'gerente'),
(5, 'funcionario', '11155566677', '123', 'Teste Funcionário', 'funcionario');

-- --------------------------------------------------------

--
-- Structure for view `emprestimo_devolucao`
--
DROP TABLE IF EXISTS `emprestimo_devolucao`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `emprestimo_devolucao`  AS  select `e`.`id` AS `emprestimo`,`e`.`dataDevolucao` AS `previsao`,`d`.`dataDevolucao` AS `devolucao`,`e`.`matriculaAluno` AS `matricula`,`e`.`dataEmprestimo` AS `dataEmprestimo` from (`emprestimo` `e` join `devolucao` `d` on((`d`.`emprestimo` = `e`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `emprestimo_livros`
--
DROP TABLE IF EXISTS `emprestimo_livros`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `emprestimo_livros`  AS  select `l`.`codLivro` AS `cod`,`l`.`titulo` AS `titulo`,`l`.`autor` AS `autor`,`l`.`editora` AS `editora`,`l`.`ano` AS `ano`,`l`.`categoria` AS `categoria`,`l`.`estoque` AS `estoque`,`ie`.`emprestimo` AS `emprestimo`,`e`.`matriculaAluno` AS `matricula`,`e`.`dataEmprestimo` AS `dataEmprestimo`,`e`.`dataDevolucao` AS `dataDevolucao`,`e`.`devolvido` AS `devolvido` from ((`itememprestimo` `ie` join `emprestimo` `e` on((`ie`.`emprestimo` = `e`.`id`))) join `livro` `l` on((`l`.`codLivro` = `ie`.`livro`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `devolucao`
--
ALTER TABLE `devolucao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emprestimo` (`emprestimo`);

--
-- Indexes for table `emprestimo`
--
ALTER TABLE `emprestimo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itememprestimo`
--
ALTER TABLE `itememprestimo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `livro` (`livro`),
  ADD KEY `emprestimo` (`emprestimo`);

--
-- Indexes for table `livro`
--
ALTER TABLE `livro`
  ADD PRIMARY KEY (`codLivro`);

--
-- Indexes for table `multa`
--
ALTER TABLE `multa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `renovacao`
--
ALTER TABLE `renovacao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emprestimo` (`emprestimo`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `devolucao`
--
ALTER TABLE `devolucao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `emprestimo`
--
ALTER TABLE `emprestimo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `itememprestimo`
--
ALTER TABLE `itememprestimo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `multa`
--
ALTER TABLE `multa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `renovacao`
--
ALTER TABLE `renovacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `devolucao`
--
ALTER TABLE `devolucao`
  ADD CONSTRAINT `devolucao_ibfk_1` FOREIGN KEY (`emprestimo`) REFERENCES `emprestimo` (`id`);

--
-- Limitadores para a tabela `itememprestimo`
--
ALTER TABLE `itememprestimo`
  ADD CONSTRAINT `itememprestimo_ibfk_1` FOREIGN KEY (`livro`) REFERENCES `livro` (`codLivro`),
  ADD CONSTRAINT `itememprestimo_ibfk_2` FOREIGN KEY (`emprestimo`) REFERENCES `emprestimo` (`id`);

--
-- Limitadores para a tabela `renovacao`
--
ALTER TABLE `renovacao`
  ADD CONSTRAINT `renovacao_ibfk_1` FOREIGN KEY (`emprestimo`) REFERENCES `emprestimo` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
