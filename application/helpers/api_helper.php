<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    function getAlunoByMatricula($matricula, $token){
		$dados = array(
			'matricula' => $matricula
		);
		$headers = [                                                                          
			"Content-type: application/json",
			"User-ID: 1",
			'Authorization:'.$token.''
		];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost/freeteca/api/aluno");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dados));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
		$result = json_decode(curl_exec($ch));
		curl_close($ch);
		return $result;
    }

    function getToken($username, $password){
		$dados = array(
            'username' => 'admin',
            'password' => 'Admin123$'
		);
		$headers = [                                                                          
			"Content-Type: application/json"
		];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost/freeteca/api/Auth/login");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dados));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$result = json_decode(curl_exec($ch));
		
		curl_close($ch);
		return $result->token;
    }
?>