<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url(); ?>/public/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>/public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url(); ?>/public/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url(); ?>/public/js/sb-admin-2.min.js"></script>
  <?php if(isset($script)){ ?>
  <script type="text/javascript" src="<?php echo $script; ?>"></script>
  <?php }?>

</body>

</html>