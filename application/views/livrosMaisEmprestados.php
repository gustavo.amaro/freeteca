<div class="card shadow mb-4" style="margin-top: 2%">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Relatório de livros mais emprestados</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Título do livro</th>
            <th>Editora</th>
            <th>Quantidade de empréstimos</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Título do livro</th>
            <th>Editora</th>
            <th>Quantidade de empréstimos</th>
          </tr>
        </tfoot>
        <tbody>
          <tr>
            <td>Tiger Nixon</td>
            <td>System Architect</td>
            <td>Edinburgh</td>
          </tr>
          <tr>
            <td>Garrett Winters</td>
            <td>Accountant</td>
            <td>Tokyo</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>