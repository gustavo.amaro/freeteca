<div class="container">
    <div class="card shadow mb-4" style="margin-top: 2%">
        <div class="card-header py-3">
            <div class="row">
                <div class="col"><a href="<?php echo base_url();?>relatorios" class="btn btn-success">Voltar</a></div>
                <div class="col"><h6 class="m-0 font-weight-bold text-primary">Atrasos</h6></div>
                <div class="col">
                    <div class="row">
                        <div class="col"></div>
                        <div class="col"><button class="btn btn-primary print"><i class="fas fa-print"></i> Imprimir</button></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                <th>Número do empréstimo</th>
                <th>Matricula do Aluno</th>
                <th>Nome do aluno</th>
                <th>Data do emprestimo</th>
                <th>Previsão de devolução</th>
                <th>Data de devolução</th>
                <th>Atraso(dias)</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                <th>Número do empréstimo</th>
                <th>Matricula do Aluno</th>
                <th>Nome do aluno</th>
                <th>Data do emprestimo</th>
                <th>Previsão de devolução</th>
                <th>Data de devolução</th>
                <th>Atraso(dias)</th>
            </tr>
            </tfoot>
            <tbody>
            <tr>
                <?php 
                foreach ($relatorio as $atraso) {
                    $aluno = getAlunoByMatricula($atraso['matricula'], $this->session->token);    
                ?>
                <td><?php echo $atraso["emprestimo"];?></td>
                <td><?php echo $atraso["matricula"];?></td>
                <td><?php echo $aluno->nome;?></td>
                <td><?php echo date_format(date_create($atraso["dataEmprestimo"]), 'd/m/Y');?></td>
                <td><?php echo date_format(date_create($atraso["previsao"]), 'd/m/Y');?></td>
                <td><?php echo date_format(date_create($atraso["devolucao"]), 'd/m/Y');?></td>
                <td><?php echo $atraso["atraso"];?></td>
            </tr>
            <?php }?>
            </tbody>
        </table>

        </div>
  </div>
</div>