<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-4 text-gray-800">Início</h1>
  <div class="row" style="margin-top: 2%">
    <div class="col"></div>
    <div class="col-md-6">
      <a class="btn btn-primary btn-block" href="<?php echo base_url();?>home/emprestimo">
        <span class="icon text-white-50">
          <i class="fas fa-book-reader"></i>
        </span>
        <span class="text">Emprestar livro</span>
      </a>
    </div>
    <div class="col"></div>
  </div>


  <div class="card shadow mb-4" style="margin-top: 2%">
    <div class="card-header py-3">
      <div class="row">
        <div class="col"><h6 class="m-0 font-weight-bold text-primary">Empréstimos ativos</h6></div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>N°</th>
                <th>Matrícula do aluno</th>
                <th>Nome do aluno</th>
                <th>Data do empréstimo</th>
                <th>Data de devolução</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>N°</th>
                <th>Matrícula do aluno</th>
                <th>Nome do aluno</th>
                <th>Data do empréstimo</th>
                <th>Data de devolução</th>
                <th>Ações</th>
              </tr>
            </tfoot>
            <tbody>
                <?php
                  foreach($emprestimos as $emprestimo){
                    $aluno = getAlunoByMatricula($emprestimo['matriculaAluno'], $this->session->token);
                ?>
              <tr>
                <td><?php echo $emprestimo['id'];?></td>
                <td><?php echo $emprestimo['matriculaAluno'];?></td>
                <td><?php echo $aluno->nome; ?></td>
                <td><?php echo date_format(date_create($emprestimo['dataEmprestimo']), 'd/m/Y');?></td>
                <td><?php echo date_format(date_create($emprestimo['dataDevolucao']), 'd/m/Y');?></td>
                <td><a href="<?php echo base_url();?>home/exibeLivrosEmprestimo/<?php echo $emprestimo['id']; ?>" class="btn btn-block btn-primary" >Ver Livros</a> <a href="<?php echo base_url();?>home/devolucao/<?php echo $emprestimo['id'];?>" class="btn btn-block btn-primary">Devolver livros</a> <a href="<?php echo base_url();?>home/renovacao/<?php echo $emprestimo['id'];?>" class="btn btn-primary btn-block">Renovar empréstimo</a></td>
              </tr>
              <?php
                  }
              ?>
            </tbody>
          </table>

        </div>
      </div>
    </div>


  </div>