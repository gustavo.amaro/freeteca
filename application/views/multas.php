<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800">Multas</h1>
    <div class="card shadow mb-4" style="margin-top: 2%">
    <div class="card-header py-3">
        <div class="row">
            <div class="col"><h6 class="m-0 font-weight-bold text-primary">Multas não pagas</h6></div>
        </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Matrícula aluno</th>
              <th>Nome</th>
              <th>Valor</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
             <th>Matrícula aluno</th>
             <th>Nome</th>
             <th>Valor</th>
             <th>Ações</th>
           </tr>
         </tfoot>
         <tbody>
          <tr>
            <?php 
              foreach ($multas as $multa) {
                $aluno = getAlunoByMatricula($multa['matriculaAluno'], $this->session->token);
            ?>
            <td><?php echo $multa["matriculaAluno"];?></td>
            <td><?php echo $aluno->nome;?></td>
            <td><?php echo $multa["valor"];?></td>
            <td><a href="<?php echo base_url();?>multas/pagarMulta/<?php echo $multa['id']?>" class="btn btn-block btn-primary" >Pagar</a></td>
          </tr>
          <?php }?>
        </tbody>
      </table>

    </div>
  </div>
</div>