<div class="container-fluid">
    
    <div class="row">
    <div class="col"></div>
    <div class="col">
        <h1 class="h3 mb-4 text-gray-800">Recibo</h1>
        <p><?php echo "Nome do Aluno: ".getAlunoByMatricula($emprestimo["matriculaAluno"], $this->session->token)->nome;?></p>
        <p><?php  echo "Data: ".date_format(date_create($emprestimo['dataEmprestimo']), 'd/m/Y');?></p>
        <p><?php echo "Devolução: ".date_format(date_create($emprestimo['dataDevolucao']), 'd/m/Y');?></p>
        <p>
        <?php
            $cont = 0;
            foreach($livros as $livro){
                $cont++;
                echo "Livro ".$cont.": ".$livro["titulo"]."<br>";
            }
        ?>
        </p>
        <button class="btn btn-primary print"><i class="fas fa-print"></i> Imprimir</button>
    </div>
    <div class="col"></div>
    </div>
</div>