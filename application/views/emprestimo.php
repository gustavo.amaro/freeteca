<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-4 text-gray-800">Empréstimo</h1>
  <div class="row">
    <div class="col"></div>
    <div class="col">
      <form id="formEmprestimo">
        <div class="form-group">
          <label for="idMatriculaAluno">Número de matricula do aluno</label>
          <input type="number" id="idMatriculaAluno" name="matriculaAluno" class="form-control">
        </div>

        <hr class="sidebar-divider my-0">

        <div class="form-group" style="margin-top: 2%">
          <label for="idTituloLivro">Código do livro</label>
          <input type="number" id="idCodLivro" class="form-control" name="codLivro">
        </div>
        <button id="addLivro" type="button" class="btn btn-success"><i class="fas fa-book-medical"></i> Adicionar livro</Button>
        <button class="btn btn-primary" id="btnEmprestar" type="submit">Emprestar livro(s)</button>
      </form>

      <div class="card shadow mb-4" style="margin-top: 2%">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Livros</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Cód.</th>
                  <th>Título do livro</th>
                  <th>Autor</th>
                  <th>Editora</th>
                </tr>
              </thead>
              <tbody id="tblLivros">
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col"></div>
  </div>
</div>