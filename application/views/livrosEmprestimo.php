<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800">Empréstimo N°<?php echo $livros[0]["emprestimo"]?></h1>
	<div class="card shadow mb-4" style="margin-top: 2%">
    <div class="card-header py-3">
      <div class="row">

        <div class="col"><a class="btn btn-success" href="<?php echo base_url();?>">Voltar</a></div><div class="col"><h6 class="m-0 font-weight-bold text-primary">Livros</h6></div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Código do livro</th>
                <th>Título</th>
                <th>Autor</th>
                <th>Editora</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Código do livro</th>
                <th>Título</th>
                <th>Autor</th>
                <th>Editora</th>
              </tr>
            </tfoot>
            <tbody>
                <?php
                  foreach($livros as $livro){
                ?>
              <tr>
                <td><?php echo $livro['cod'];?></td>
                <td><?php echo $livro['titulo'];?></td>
                <td><?php echo $livro['autor'];?></td>
                <td><?php echo $livro['editora'];?></td>
              </tr>
              <?php
                  }
              ?>
            </tbody>
          </table>

        </div>
      </div>
    </div>
</div>