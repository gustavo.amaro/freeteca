<div class="container-fluid">
    <div class="card shadow mb-4" style="margin-top: 2%">
        <div class="card-header py-3">
        <div class="row">
            <div class="col"><a class="btn btn-success" href="<?php echo base_url();?>">Voltar</a></div><div class="col"><h6 class="m-0 font-weight-bold text-primary">Mensagem</h6></div>
        </div>
        <div class="card-body">
            <h3><?php echo $message; ?></h3>
        </div>
    </div>
</div>