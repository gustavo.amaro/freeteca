<div class="container-fluid">
	<h1 class="h3 mb-4 text-gray-800">Relatórios</h1>
	<div class="row">
    <div class="col"></div>
    <div class="col-md-6">
      <form action="<?php echo base_url();?>relatorios/gerarRelatorio" method="post">
        <div class="form-group">
          <label>Tipo de relatório</label>
          <select class="form-control form-control-lg" name="tipo">
            <option value="emprestados">Livros mais emprestados</option>
            <option value="atrasos">Atrasos de devolução</option>
          </select>
        </div>
        <div class="form-group">
          <label>Mês</label>
          <select class="form-control form-control-lg" name="mes">
            <option value="1">Janeiro</option>
            <option value="2">Fevereiro</option>
            <option value="3">Março</option>
            <option value="4">Abril</option>
            <option value="5">Maio</option>
            <option value="6">Junho</option>
            <option value="7">Julho</option>
            <option value="8">Agosto</option>
            <option value="9">Setembro</option>
            <option value="10">Outubro</option>
            <option value="11">Novembro</option>
            <option value="12">Dezembro</option>
          </select>
        </div>
        <div class="form-group">
          <label>Ano</label>
          <select class="form-control form-control-lg" name="ano">
            <?php
              
              foreach($anos as $ano){
                echo "<option value='".$ano['ano']."'>".$ano['ano']."</option>";
              }
            ?>
          </select>
        </div>
        <button type="submit" class="btn btn-block btn-primary">Gerar Relatório</button>
      </form>
    </div>
    <div class="col"></div>
  </div>
</div>