<div class="container-fluid">
	<h1 class="h3 mb-4 text-gray-800">Usuários</h1>
	<div class="row" style="margin-top: 2%">
    <div class="col"></div>
    <div class="col-md-6">
      <a href="<?php echo base_url();?>usuarios/cadastroUsuario" class="btn btn-primary btn-block">
        <span class="icon text-white-50">
          <i class="fas fa-user-plus"></i>
        </span>
        <span class="text">Cadastrar usuário</span>
      </a>
    </div>
    <div class="col"></div>
  </div>
  <div class="card shadow mb-4" style="margin-top: 2%">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Usuários Cadastrados</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nome</th>
              <th>CPF</th>
              <th>Função</th>
              <th>Nome de usuário</th>
              <th>Senha</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
             <th>Nome</th>
             <th>CPF</th>
             <th>Função</th>
             <th>Nome de usuário</th>
             <th>Senha</th>
             <th>Ações</th>
           </tr>
         </tfoot>
         <tbody>
         <?php 
          foreach ($usuarios as $usuario) {?>
          <tr>
            <td><?php echo $usuario['nome'];?></td>
            <td><?php echo $usuario['cpf'];?></td>
            <td><?php echo $usuario['funcao'];?></td>
            <td><?php echo $usuario['nomeUsuario'];?></td>
            <td><?php echo $usuario['senha'];?></td>
            <td><a href="<?php echo base_url();?>usuarios/atualizarUsuario/<?php echo $usuario['id']?>" class="btn btn-block btn-primary" >Alterar</a> <a href="<?php echo base_url();?>usuarios/deletarUsuario/<?php echo $usuario['id']?>" class="btn btn-block btn-danger">Excluir</a></td>
          </tr>
          <?php }?>
        </tbody>
      </table>

    </div>
  </div>
</div>
</div>