<div class="container-fluid">
	<h1 class="h3 mb-4 text-gray-800"><?php echo isset($codLivro)?"Alterar livro":"Cadastro de livro";?></h1>
  	<div class="row" style="margin-top: 2%">
  		<div class="col"></div>
  		<div class="col-md-6">
  			<form action="<?php echo isset($codLivro)?base_url().'livros/alterarLivro':base_url().'livros/cadastrarLivro';?>" method="POST">
  				<div class="form-row">
  					<div class="form-group col-md-6">
	  					<label for="idCodigo">Código do livro: </label>
	  					<input type="text" class="form-control" <?php echo isset($codLivro)?'readonly':'';?> name="codLivro" id="idCodigo" value="<?php echo isset($codLivro)?$codLivro:'';?>">
	  				</div>
	  				<div class="form-group col-md-6">
	  					<label for="idTitulo">Titulo do livro: </label>
	  					<input type="text" class="form-control" name="titulo" id="idTitulo" value="<?php echo isset($codLivro)?$livro->titulo:''; ?>">
	  				</div>
  				</div>
  				<div class="form-group">
  					<label for="idAutor">Autor: </label>
  					<input type="text" class="form-control" name="autor" id="idAutor" value="<?php echo isset($codLivro)?$livro->autor:'';?>">
  				</div>
  				<div class="form-group">
  					<label for="idEditora">Editora: </label>
  					<input type="text" class="form-control" name="editora" id="idEditora" value="<?php echo isset($codLivro)?$livro->editora:'';?>">
  				</div>
  				<div class="form-group">
  					<label for="idAno">Ano de lançamento: </label>
  					<input type="number" class="form-control" name="ano" id="idAno" value="<?php echo isset($codLivro)?$livro->ano:'';?>">
  				</div>
  				<div class="form-group">
  					<label for="idCategoria">Categoria: </label>
  					<input type="text" class="form-control" name="categoria" id="idCategoria" value="<?php echo isset($codLivro)?$livro->categoria:'';?>">
  				</div>
  				<div class="form-row">
	  				<div class="form-group col">
	  					<label for="idPaginas">Paginas: </label>
	  					<input type="number" class="form-control" name="qtdPaginas" id="idPaginas" value="<?php echo isset($codLivro)?$livro->qtdPaginas:'';?>">
	  				</div>
	  				<div class="form-group col">
	  					<label for="idEstoque">Estoque: </label>
	  					<input type="number" class="form-control" name="estoque" id="idEstoque" value="<?php echo isset($codLivro)?$livro->estoque:'';?>">
	  				</div>
  				</div>
  				<button class="btn btn-primary btn-block" type="submit"><?php echo isset($codLivro)?"Atualizar":"Cadastrar";?></button>
  			</form>
  		</div>
  		<div class="col"></div>
  	</div>
</div>