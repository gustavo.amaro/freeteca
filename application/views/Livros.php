<div class="container-fluid">
	<h1 class="h3 mb-4 text-gray-800">Livros</h1>
	<div class="row" style="margin-top: 2%">
    <div class="col"></div>
    <div class="col-md-6">
      <a href="<?php echo base_url();?>livros/cadastroLivro" class="btn btn-primary btn-block">
        <span class="icon text-white-50">
          <i class="fas fa-book-medical"></i>
        </span>
        <span class="text">Cadastrar livro</span>
      </a>
    </div>
    <div class="col"></div>
  </div>
  <div class="card shadow mb-4" style="margin-top: 2%">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Livros Cadastrados</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Cód. do livro</th>
              <th>Título</th>
              <th>Editora</th>
              <th>Ano de publicação</th>
              <th>Categoria</th>
              <th>Páginas(quantidade)</th>
              <th>Estoque</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
             <th>Cód. do livro</th>
             <th>Título</th>
             <th>Editora</th>
             <th>Ano de publicação</th>
             <th>Categoria</th>
             <th>Páginas(quantidade)</th>
             <th>Estoque</th>
             <th>Ações</th>
           </tr>
         </tfoot>
         <tbody>
          <tr>
            <?php 
              foreach ($livros as $livro) {?>
            <td><?php echo $livro["codLivro"];?></td>
            <td><?php echo $livro["titulo"];?></td>
            <td><?php echo $livro["editora"];?></td>
            <td><?php echo $livro["ano"];?></td>
            <td><?php echo $livro["categoria"];?></td>
            <td><?php echo $livro["qtdPaginas"];?></td>
            <td><?php echo $livro["estoque"];?></td>
            <td><a href="<?php echo base_url();?>livros/atualizarLivro/<?php echo $livro['codLivro']?>" class="btn btn-block btn-primary" >Alterar</a> <a href="<?php echo base_url();?>livros/deletarLivro/<?php echo $livro['codLivro']?>" class="btn btn-block btn-danger">Excluir</a></td>
          </tr>
          <?php }?>
        </tbody>
      </table>

    </div>
  </div>
</div>