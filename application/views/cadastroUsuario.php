<div class="container-fluid">
	<h1 class="h3 mb-4 text-gray-800"><?php echo isset($id)?"Alterar Usuário":"Cadastro de usuário";?></h1>
	<div class="row" style="margin-top: 2%">
  		<div class="col"></div>
  		<div class="col-md-6">
			<form action="<?php echo isset($id)?base_url().'usuarios/alterarUsuario/'.$id:base_url().'usuarios/cadastrarUsuario';?>" method="POST">
				<div class="form-group">
					<label for="idNumeroMatricula">Nome: </label>
					<input type="text" class="form-control" id="idNome" name="nome" value="<?php echo isset($id)?$usuario->nome:''; ?>">
				</div>
				<div class="form-group">
					<label for="idCpf">CPF: </label>
					<input type="text" class="form-control" id="idCpf" name="cpf" value="<?php echo isset($id)?$usuario->cpf:''; ?>">
				</div>
				<div class="form-group">
					<label for="idNomeUsuario">Nome de usuário: </label>
					<input type="text" class="form-control" id="idNomeUsuario" name="nomeUsuario" value="<?php echo isset($id)?$usuario->nomeUsuario:''; ?>">
				</div>
				<div class="form-group">
					<label for="idSenha">Senha: </label>
					<input type="password" class="form-control" id="idSenha" name="senha" value="<?php echo isset($id)?$usuario->senha:''; ?>">
				</div>
				<div class="form-group">
					<label for="idFuncao">Função: </label>
					<select id="idFuncao" name="funcao" class="form-control form-control-lg">
					<?php if($usuario->funcao == "funcionario"){?>
						<option value="funcionario">Funcionário</option>
						<option value="gerente">Gerente</option>
					<?php }else{ ?>
						<option value="gerente">Gerente</option>
						<option value="funcionario">Funcionário</option>
					<?php }?>
					</select>
				</div>
				<button class="btn btn-primary btn-block" type="submit"><?php echo isset($id)?"Atualizar":"Cadastrar";?></button>
			</form>
		</div>
		<div class="col"></div>
	</div>
</div>