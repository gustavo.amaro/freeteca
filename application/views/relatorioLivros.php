<div class="container">
    <div class="card shadow mb-4" style="margin-top: 2%">
        <div class="card-header py-3">
            <div class="row">
                <div class="col"><a href="<?php echo base_url();?>relatorios" class="btn btn-success">Voltar</a></div>
                <div class="col"><h6 class="m-0 font-weight-bold text-primary">Livros mais emprestados</h6></div>
                <div class="col">
                    <div class="row">
                        <div class="col"></div>
                        <div class="col"><button class="btn btn-primary print"><i class="fas fa-print"></i> Imprimir</button></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                <th>Cód. Livro</th>
                <th>Título</th>
                <th>Autor</th>
                <th>Editora</th>
                <th>Ano</th>
                <th>Categoria</th>
                <th>Quantidade de empréstimos</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                <th>Cód. Livro</th>
                <th>Título</th>
                <th>Autor</th>
                <th>Editora</th>
                <th>Ano</th>
                <th>Categoria</th>
                <th>Quantidade de empréstimos</th>
            </tr>
            </tfoot>
            <tbody>
            <tr>
                <?php 
                foreach ($relatorio as $livro) {?>
                <td><?php echo $livro["cod"];?></td>
                <td><?php echo $livro["titulo"];?></td>
                <td><?php echo $livro["autor"];?></td>
                <td><?php echo $livro["editora"];?></td>
                <td><?php echo $livro["ano"];?></td>
                <td><?php echo $livro["categoria"];?></td>
                <td><?php echo $livro["qtd"];?></td>
            </tr>
            <?php }?>
            </tbody>
        </table>

        </div>
  </div>
</div>