<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('livros_model');
		$this->load->model('emprestimos_model');
		$this->load->model('devolucoes_model');
		$this->load->model('renovacoes_model');
		$this->load->model('multas_model');
		$this->load->model('usuarios_model');
	}
	public function index(){
		if(isset($this->session->usuario)){
			$emprestimos = $this->emprestimos_model->exibeEmprestimos();
			$data["emprestimos"] = $emprestimos;
			$this->template->show('home', $data);
		}else{
			header("location: ".base_url()."home/login");
		}
	}
	public function emprestimo(){
		if(isset($this->session->usuario)){
			$data["script"] = base_url()."public/js/util.js";
			$this->template->show('emprestimo', $data);
		}else{
			header("location: ".base_url()."home/login");
		}
	}
	public function adicionaLivroEmprestimo(){
		if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}
		$json = array();
		$data = $this->input->post();
		$json["status"] = 0;
		$livro = $this->livros_model->getLivroByCod($data["codLivro"]);
		if($livro != null){
			$json["codLivro"] = $livro->codLivro;
			$json["titulo"] = $livro->titulo;
			$json["autor"] = $livro->autor;
			$json["editora"] = $livro->editora;
			$json["status"] = 1;
		}
		echo json_encode($json);
	}

	public function emprestarLivros(){
		if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}
		$json = array();
		$data = $this->input->post();
		$json["status"] = 1;
		$json["str"] = $this->livros_model->verificaEstoque($data['livros']);
		if(getAlunoByMatricula($data['matriculaAluno'], $this->session->token) != null){
			if(getAlunoByMatricula($data['matriculaAluno'], $this->session->token)->matriculado == 1){
				if(!$this->multas_model->haMulta($data['matriculaAluno'])){
					if(!$this->emprestimos_model->haEmprestimos($data['matriculaAluno'])){
						if($json["str"] == ''){
							if(!$this->emprestimos_model->registraEmprestimo($data["livros"], $data["matriculaAluno"])){
								$json["status"] = 0;
							}
						}else{
							$json["status"] = 4;
						}
					}else{
						$json["status"] = 3;
					}
				}else{
					$json["status"] = 2;
				}
			}else{
				$json["status"] = 5;
			}
		}else{
			$json["status"] = 6;
		}
		echo json_encode($json);
	}
	public function geraRecibo(){
		$data["emprestimo"] = $this->emprestimos_model->getLastEmprestimo();
		$data["livros"] = $this->emprestimos_model->exibeLivros($data["emprestimo"]["id"]);
		$data["script"] = base_url()."public/js/util.js";
		$this->template->show("recibo",$data);
	}

	public function exibeLivrosEmprestimo($idEmprestimo){
		if(isset($this->session->usuario)){
			$dados["livros"] = $this->emprestimos_model->exibeLivros($idEmprestimo);
			$this->template->show("livrosEmprestimo",$dados);
		}else{
			header("location: ".base_url()."home/login");
		}
	}

	public function devolucao($idEmprestimo, $verificador=true){
		$this->emprestimos_model->devolverLivros($idEmprestimo);
		$livros = $this->emprestimos_model->exibeLivros($idEmprestimo);
		$emprestimo = $this->emprestimos_model->getEmprestimoById($idEmprestimo);
		foreach($livros as $livro){
			$this->livros_model->incrementaEstoque($livro['cod']);
		}
		$message = "Erro ao tentar realizar devolução!";
		if($this->devolucoes_model->registraDevolucao($idEmprestimo)){
			$message = "Devolução realizada com sucesso!";
		}
		if(strtotime($emprestimo->dataDevolucao) < strtotime(date('y-m-d'))){
			$dataDevolucao = new DateTime($emprestimo->dataDevolucao);
			$dataAtual = new DateTime();
			$dataAtual->diff($dataDevolucao);
			$dias = $dataAtual->diff($dataDevolucao)->days;
			if($this->multas_model->registraMulta($emprestimo->matriculaAluno, $dias))
				$message = "Devolução realizada!<br>O aluno atrasou em ".$dias." dias por isso foi registrado multa de R$".number_format($dias, 2, ',', '.').".";
		}
		if($verificador)
			$this->messageView($message);
	}

	public function renovacao($idEmprestimo){
		$message = '';
		$emprestimo = $this->emprestimos_model->getEmprestimoById($idEmprestimo);
		$dataAtual = new DateTime();
		if(strtotime($emprestimo->dataDevolucao) < strtotime(date('y-m-d'))){
			$this->devolucao($idEmprestimo, false);
			$dataDevolucao = new DateTime($emprestimo->dataDevolucao);
			$dias = $dataAtual->diff($dataDevolucao)->days;
			$message = "Não foi possível realizar a renovação!<br>O aluno atrasou em ".$dias." dias por isso foi registrado multa de R$".number_format($dias, 2, ',', '.').".<br>Foi registrado a devolução!";
		}else{
			if($this->renovacoes_model->numRenovacoes($idEmprestimo) < 3){
				$livros = $this->emprestimos_model->exibeLivros($idEmprestimo);
				$renova = true;
				foreach($livros as $livro){
					if($livro["estoque"] <= 1){
						$renova = false;
						$message = $message."Não foi possível renovar.<br>O livro: '".$livro["titulo"]."' está sem estoque!";
					}
				}
				if($renova){
					$this->emprestimos_model->renovarEmprestimo($idEmprestimo);
					$this->renovacoes_model->registraRenovacao($idEmprestimo);
					$dataAtual->add(new DateInterval("P7D"));
					$message = "Renovação realizada com sucesso!<br>Nova data de devolução: ".$dataAtual->format("d/m/Y");
				}
			}else{
				$message = "Não foi possível realizar a renovação!<br>O empréstimo já foi renovado 3 vezes.";
			}
		}
		$this->messageView($message);
	}

	public function messageView($message){
		if(isset($this->session->usuario)){
			$data["message"] = $message;
			$this->template->show("message",$data);
		}else{
			header("location: ".base_url()."home/login");
		}
	}

	public function logar(){
		$user = $this->input->post("usuario");
		$pass = $this->input->post("senha");
		$usuario = $this->usuarios_model->logar($user, $pass);
		if($usuario != null){
			$this->session->usuario = $usuario->nome;
			$this->session->funcao = $usuario->funcao;
			$this->session->token = getToken("admin","Admin123$");
			header("location: ".base_url()."");
		}else{
			$data["status"] = "Usuário e/ou senha incorreto(s).";
			$this->load->view("login", $data);
		}
	}

	public function logout(){
		$this->session->unset_userdata('usuario');
		$this->session->unset_userdata('funcao');
		$this->session->unset_userdata('token');
		header("location: ".base_url()."");
	}

	public function login(){
		$this->load->view("login");
	}
}
