<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livros extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('livros_model');
	}
	public function index(){
		if(isset($this->session->usuario)){
			$data["livros"] = $this->livros_model->getLivros();
			$this->template->show('livros', $data);
		}else{
			header("location: ".base_url()."home/login");
		}
	}
	public function cadastroLivro(){
		if(isset($this->session->usuario)){
			$this->template->show('cadastroLivro');
		}else{
			header("location: ".base_url()."home/login");
		}
	}
	public function atualizarLivro($codLivro){
		if(isset($this->session->usuario)){
			$data["codLivro"] = $codLivro;
			$data["livro"] = $this->livros_model->getLivroByCod($codLivro);
			$this->template->show('cadastroLivro', $data);
		}else{
			header("location: ".base_url()."home/login");
		}
	}
	
	public function cadastrarLivro(){
		if(isset($this->session->usuario)){
			$data = $this->input->post();
			$this->livros_model->insert($data);
			header("location: ".base_url()."livros");
		}else{
			header("location: ".base_url()."home/login");
		}
	}
	public function deletarLivro($codLivro){
		if(isset($this->session->usuario)){
			$this->livros_model->delete($codLivro);
			header("location: ".base_url()."livros");
		}else{
			header("location: ".base_url()."home/login");
		}
	}
	public function alterarLivro(){
		if(isset($this->session->usuario)){
			$data = $this->input->post();
			$this->livros_model->update($data["codLivro"], $data);
			header("location: ".base_url()."livros");
		}else{
			header("location: ".base_url()."home/login");
		}
	}
}
