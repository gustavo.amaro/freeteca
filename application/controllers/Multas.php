<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Multas extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('multas_model');
    }

    public function index(){
        if(isset($this->session->usuario)){
			$data["multas"] = $this->multas_model->getMultas();
            $this->template->show("multas", $data);
		}else{
			header("location: ".base_url()."home/login");
		}
    }
    public function pagarMulta($idMulta){
        if(isset($this->session->usuario)){
			$data['message'] = '';
            if($this->multas_model->pagarMulta($idMulta)){
                $data['message'] = "Multa quitada com sucesso!<br>O aluno já pode fazer novo empréstimo.";
            }else{
                $data['message'] = "Erro ao tentar quitar multa.";
            }
            $this->template->show("message",$data);
		}else{
			header("location: ".base_url()."home/login");
		}
    }
}
?>