<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('relatorios_model');
	}
	public function index()
	{
		if(isset($this->session->usuario)){
			$data["anos"] = $this->relatorios_model->getAnos();
			$this->template->show('relatorios', $data);
		}else{
			header("location: ".base_url()."home/login");
		}	
	}

	public function gerarRelatorio(){
		$data = null;
		$tipo = $this->input->post('tipo');
		$mes = $this->input->post('mes');
		$ano = $this->input->post('ano');
		$data["script"] = base_url()."public/js/util.js";
		if($tipo == 'atrasos'){
			$data["relatorio"] = $this->relatorios_model->atrasosDevolucao($mes, $ano);
			$this->template->show("relatorioAtraso", $data);
		}else if($tipo=='emprestados'){
			$data["relatorio"] = $this->relatorios_model->livrosMaisEmprestados($mes, $ano);
			$this->template->show("relatorioLivros", $data);
		}
	}
}
