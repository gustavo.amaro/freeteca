<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('usuarios_model');
	}

	public function index()
	{
		if(isset($this->session->usuario)){
			$data['usuarios'] = $this->usuarios_model->getUsuarios();
			$this->template->show('usuarios', $data);
		}else{
			header("location: ".base_url()."home/login");
		}
	}
	public function cadastroUsuario(){
		if(isset($this->session->usuario)){
			$this->template->show('cadastroUsuario');
		}else{
			header("location: ".base_url()."home/login");
		}	
	}
	public function atualizarUsuario($id){
		if(isset($this->session->usuario)){
			$data["id"] = $id;
			$data["usuario"] = $this->usuarios_model->getUsuarioById($id);
			$this->template->show('cadastroUsuario', $data);
		}else{
			header("location: ".base_url()."home/login");
		}
	}

	public function cadastrarUsuario(){
		$data = $this->input->post();
		$this->usuarios_model->insert($data);
		header("location: ".base_url()."usuarios");
	}
	public function alterarUsuario($id){
		$data = $this->input->post();
		$this->usuarios_model->update($id, $data);
		header("location: ".base_url()."usuarios");
	}
	public function deletarUsuario($id){
		$this->usuarios_model->delete($id);
		header("location: ".base_url()."usuarios");
	}
}
