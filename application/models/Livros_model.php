<?php

class Livros_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getLivros(){
		$this->db->from('livro');
		$this->db->order_by('titulo');
		return $this->db->get()->result_array();
	}
	public function getLivroByCod($codLivro){
		$this->db->from('livro');
		$this->db->where("codLivro", $codLivro);
		return $this->db->get()->row();
	}
	public function insert($data){
		$this->db->insert("livro", $data);
	}

	public function update($codLivro, $data) {
		$this->db->where("codLivro", $codLivro);
		$this->db->update("livro", $data);
	}

	public function delete($codLivro) {
		$this->db->where("codLivro", $codLivro);
		$this->db->delete("livro");
	}
	public function decrementaEstoque($codLivro){
		$this->db->set('estoque', 'estoque-1',false);
		$this->db->where('codLivro', $codLivro);
		$this->db->update("livro");
	}
	public function incrementaEstoque($codLivro){
		$this->db->set('estoque','estoque+1',false);
		$this->db->where('codLivro',$codLivro);
		$this->db->update("livro");
	}
	public function verificaEstoque($livros){
		$retorno = "";
		foreach($livros as $livro){
			$this->db->from('livro');
			$this->db->where('codLivro',$livro);
			$result = $this->db->get()->row();
			if($result->estoque <= 1){
				$retorno = $retorno." Não é permitido o empréstimo do livro: '$result->titulo' pois há somente um exemplar em estoque.";
			}
		}
		return $retorno;
	}
}