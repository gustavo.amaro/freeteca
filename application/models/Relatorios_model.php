<?php

class Relatorios_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
    }

    public function atrasosDevolucao($mes, $ano){
        $sql = "SELECT *, (devolucao-previsao) as atraso FROM emprestimo_devolucao WHERE previsao<devolucao AND MONTH(dataEmprestimo) = '$mes' AND YEAR(dataEmprestimo) = '$ano' order by atraso desc";
        return $this->db->query($sql)->result_array();
    }
    public function getAnos(){
        $sql = "SELECT DISTINCT(YEAR(dataEmprestimo)) ano FROM emprestimo;";
        return $this->db->query($sql)->result_array();
    }
    public function livrosMaisEmprestados($mes, $ano){
        $sql = "SELECT *, count(cod) as qtd FROM emprestimo_livros WHERE MONTH(dataEmprestimo) = '$mes' AND YEAR(dataEmprestimo) = '$ano' GROUP BY cod ORDER BY qtd DESC LIMIT 20;";
        return $this->db->query($sql)->result_array();
    }
}
?>