<?php

class Emprestimos_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model("livros_model");
	}

	public function registraEmprestimo($livros, $matriculaAluno){
		$result = true;
		$sql = "INSERT INTO emprestimo VALUES (default, '".$matriculaAluno."', now(), ADDDATE(now(), INTERVAL 7 DAY), default)";
		if($this->db->query($sql)){
			$idEmprestimo = $this->db->query("SELECT max(id) as id FROM emprestimo")->row()->id;
			foreach($livros as $livro){
				$sql = "INSERT INTO itememprestimo VALUES (default, '".$livro."', '".$idEmprestimo."')";
				if($this->db->query($sql)){
					$this->livros_model->decrementaEstoque($livro);
				}else{
					$result = false;
				}
			}
		}else{
			$result = false;
		}

		return $result;
	}
	public function haEmprestimos($matriculaAluno){
		$sql = "SELECT * FROM emprestimo where matriculaAluno='$matriculaAluno' and devolvido='0'";
		return $this->db->query($sql)->num_rows() > 0;
	}

	public function getEmprestimoById($idEmprestimo){
		$this->db->from('emprestimo');
		$this->db->where('id',$idEmprestimo);
		return $this->db->get()->row();
	}
	public function exibeEmprestimos(){
		$sql = "SELECT * FROM emprestimo WHERE devolvido = 0";
		return $this->db->query($sql)->result_array();
	}

	public function exibeLivros($idEmprestimo){
		$sql = "SELECT * FROM emprestimo_livros WHERE emprestimo=$idEmprestimo";
		return $this->db->query($sql)->result_array();
	}
	public function devolverLivros($idEmprestimo){
		$this->db->set("devolvido",'1',false);
		$this->db->where("id", $idEmprestimo);
		$this->db->update("emprestimo");
	}
	public function renovarEmprestimo($idEmprestimo){
		$sql = "UPDATE emprestimo SET dataDevolucao = ADDDATE(now(), INTERVAL 7 DAY) WHERE id=$idEmprestimo";
		return $this->db->query($sql);
	}
	public function getLastEmprestimo(){
		$sql = "SELECT * FROM emprestimo WHERE id=(SELECT max(id) FROM emprestimo)";
		return $this->db->query($sql)->row_array();
	}
}

?>