<?php

class Usuarios_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getUsuarios(){
		$this->db->from('usuario');
		$this->db->order_by('nome');
		return $this->db->get()->result_array();
	}
	public function getUsuarioById($id){
		$this->db->from('usuario');
		$this->db->where('id',$id);
		return $this->db->get()->row();
	}
	public function insert($data){
		$this->db->insert('usuario',$data);
	}
	public function update($id, $data){
		$this->db->where("id",$id);
		$this->db->update("usuario",$data);
	}
	public function delete($id){
		$this->db->where("id",$id);
		$this->db->delete("usuario");
	}
	public function logar($usuario, $senha){
		$sql = "SELECT * FROM usuario WHERE nomeUsuario='$usuario' AND senha='$senha'";
		return $this->db->query($sql)->row();
	}
}