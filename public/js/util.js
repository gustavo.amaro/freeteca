$(document).ready(function(){
	var livrosDoEmprestimo = [];
	var idLivro = $("#idCodLivro");

	$("#addLivro").click(function(){
		$.ajax({
			url: "http://localhost/freeteca/home/adicionaLivroEmprestimo",
			dataType: "json",
			type: "POST",
			data: {codLivro: idLivro.val()},
			beforeSend: function() {
				$('#addLivro').html('Adicionando...');
			},
			success: function(response) {
				$('#addLivro').html('<i class="fas fa-book-medical"></i> Adicionar livro');
				var str;
				for(i=0; i < livrosDoEmprestimo.length; i++){
					if(livrosDoEmprestimo[i] == response["codLivro"]){
						response["status"] = 2;
					}
				}
				if(response["status"] == 1){
					str ="<tr><td>"+response["codLivro"]+"</td><td>"+response["titulo"]+"</td><td>"+response["autor"]+"</td><td>"+response["editora"]+"</td>";
					tblLivrosAtual = $('#tblLivros').html();
					$('#tblLivros').html(tblLivrosAtual + str);
					livrosDoEmprestimo.push(response["codLivro"]);
				}else if(response["status"] == 2){
					alert("livro já adicionado!");
				}else{
					alert("O código do livro indicado não existe no banco de dados!");
				}
			},
			error: function() {
				$('#addLivro').html('Erro!');
			}
		});
	});
	function limparCampos(){
		$("#idMatriculaAluno").val('');
		livrosDoEmprestimo = [];
		$("#idCodLivro").val('');
		$('#tblLivros').html('');
	}

	$("#formEmprestimo").submit(function(event){
		event.preventDefault();
		var matricula = $("#idMatriculaAluno");
   		if(livrosDoEmprestimo.length > 0){
   			if(matricula.val()!=''){
   				$.ajax({
   					url: "http://localhost/freeteca/home/emprestarLivros",
   					dataType: "json",
   					type: "POST",
   					data: {matriculaAluno: matricula.val(), livros: livrosDoEmprestimo},
   					beforeSend: function(){
   						$("#btnEmprestar").html("Aguarde...");
   					},
   					success: function(response){
						if(response["status"]==1){
							limparCampos();
							window.location.href = "http://localhost/freeteca/home/geraRecibo/";
						}else if(response["status"]==2){
							alert('Não foi possível realizar o empréstimo pois o aluno tem multas pendentes.');
							limparCampos();
						}else if(response["status"]==3){
							alert('Este aluno possui um empréstimo não devolvido.');
							limparCampos();
						}else if(response["status"]==4){
							alert(''+response['str']);
							limparCampos();
						}else if(response["status"]==5){
							alert('O Aluno não possui matrícula ativa.');
							limparCampos();
						}else if(response["status"]==6){
							alert('Código de matrícula inexistente.');
							limparCampos();
						}else{
							alert("Erro ao registrar empréstimo.");
							limparCampos();
						}
						$("#btnEmprestar").html("Emprestar livro(s)");
   					},
   					error: function(){
   						$("#btnEmprestar").html(matricula.val());
   					}
   				});
   			}else{
   				alert('Digite o número de matrícula do aluno!');
   			}
   		}else{
   			alert('Tem que haver livros para realizar o empréstimo!');
   		}
	});

	$(".print").click(function(){
		print();
	});
});